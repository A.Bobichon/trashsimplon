<?php

namespace app\controller;

use security\config\BDD;

class userController {

    function CreateUser($post){  

    $username = htmlspecialchars($post["username"]);
    $email = htmlspecialchars($post["email"]);
    $password = htmlspecialchars($post["password"]);
    
    $db = new BDD(
        $_ENV["IP_ADRESS"],
        $_ENV["USERDB"],
        $_ENV["PASSWORD"]
    );

    $cipher = $_ENV["CIPHER"];
    $hashed_password = hash("sha3-256", $password);
    $salt = uniqid(mt_rand(), true);
    $poivre = $_ENV["POIVRE"];

    $cryptPassword = openssl_encrypt( $hashed_password, $cipher, $salt );
    $cryptPasswordPoivre = openssl_encrypt( $cryptPassword, $cipher, $poivre );

    $cryptEmail = openssl_encrypt( $email, $cipher, $salt );
    $cryptEmailPoivre = openssl_encrypt( $cryptEmail, $cipher, $poivre );

    $pdo = $db->Connection();

    if ( $password !== "" ){
        $pdo->exec( 
        "INSERT INTO security.userSec (username, email, user_password, user_salt)
         VALUES ( '" . $username ."'  ,'" . $cryptEmailPoivre ."'  , '" . $cryptPasswordPoivre . "' , '" . $salt ."');"
        );
    }
    }
}