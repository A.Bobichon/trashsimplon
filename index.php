<?php

require './vendor/autoload.php';
require './routeur/routeur.php';

use security\routeur\Routeur;
use security\config\BDD;
use security\Query\initTable;

$table = new initTable();
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$request = $_SERVER['REQUEST_URI'];

var_dump($request);

// SET BDD
$db = new BDD(
    $_ENV["IP_ADRESS"],
    $_ENV["USERDB"],
    $_ENV["PASSWORD"]
);

// CONNECT TO THE BDD
$pdo = $db->Connection();
$pdo->prepare($table->CreateTable())
    ->execute();

$routeur = new Routeur();
$routeur->initRouteur($request);




