<?php

namespace security\Query;

class initTable {

    function dropTable(){
        return "DROP TABLE IF EXISTS userSec";
    }

    function CreateTable(){
        return "CREATE TABLE IF NOT EXISTS  userSec (
                id INT AUTO_INCREMENT PRIMARY KEY,
                email VARCHAR(255) NOT NULL,
                username VARCHAR(255) NOT NULL,
                user_password VARCHAR(255) NOT NULL,
                user_salt VARCHAR(255) NOT NULL
            );";
    }
}