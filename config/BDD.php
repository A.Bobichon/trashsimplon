<?php

namespace security\config;

use PDO;

class BDD {

    private $dsn;
    private $user;
    private $password;

   function __construct($dsn, $user, $password)
   {
       $this->dsn = $dsn;
       $this->user = $user;
       $this->password = $password;
   }

    function Connection(){
       $pdo = new PDO($this->dsn, $this->user, $this->password);
       $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       return $pdo;
    }
}